
module.exports = {
  'reasons': [
    `All you need is love. But a little Fediverse now and then is good for you too`,
    `Everyone joins Fediverse in the end. Resistance is futile`,
    `When you join Fediverse, the Force is with you`,
    `Fediverse is like a box of chocolates. You never know what you're gonna get`,
    `Imperfection is beauty. Fediverse is imperfect. Thus, it is beautiful`,
    `Fediverse is blessed every day with 1440 sunsets`,
    `He who has overcome his fears will be in Fediverse`,
    `Fediverse is a riddle wrapped in a mystery inside an enigma`,
    `The answer is out there, Neo, and it's looking for you, and it will find you in Fediverse`,
    `Fediverse lets you travel without moving your feet`,
    `This life's hard, but it's harder if you're not in Fediverse`,
    `There is nothing in the world so irresistibly contagious as Fediverse`,
    `Fediverse is an exercise bicycle for the mind. It might not take you anywhere, but it tones up the muscles that can`,
    `When in doubt, join Fediverse`,
    `It is the unexplored that attracts us. Join Fediverse and become its daring explorer`,
    `Fediverse is the bridge between you and the rest of the world`,
    `By joining Fediverse you change the rules of the game`,
    `Fediverse teaches to always be a little defiant, to question and to doubt`
  ]
}
