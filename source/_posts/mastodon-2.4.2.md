
---
layout: "post"
title: "Mastodon 2.4.2"
date: 2018-06-19
tags:
    - mastodon
preview: "This release adds autofollow option to invites, preferred languages choice, default language instead of auto-detect, UI improvements and minor fixes"
url: "https://github.com/tootsuite/mastodon/releases/tag/v2.4.2"
lang: en
---

This release brings autofollow option to invites, which can be useful for bringing followers over from other platforms.
Language opt-out is replaced with opt-in: users can now select the languages they want to see. Manual default posting language option is added next to auto-detect. The release includes some UI improvements and minor fixes.
Read full changelog [here](https://github.com/tootsuite/mastodon/releases/tag/v2.4.2).
