
---
layout: "post"
title: "Hubzilla 3.4 release"
date: 2018-05-05
tags:
    - hubzilla
preview: "This release is focused on usability improvements. It introduces a new permission role and autosave functionality, along with other changes."
url: "https://hub.somaton.com/channel/mario/?f=&mid=97a00196d77da087503cbde7a7e7c43fb59655c104d858519544d510068fccc1@hub.somaton.com"
lang: en
---

This release is focused on usability improvements. It introduces a new permission role and autosave functionality for posts and comments, along with other changes.
Read full changelog [here](https://hub.somaton.com/channel/mario/?f=&mid=97a00196d77da087503cbde7a7e7c43fb59655c104d858519544d510068fccc1@hub.somaton.com).
