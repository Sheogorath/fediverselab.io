
---
layout: "post"
title: "FAQ for newcomers"
date: 2018-02-15
tags:
    - fediverse
preview: "this is a stub for an article. Anyone welcome to contribute"
url: "/en/post/FAQ-for-newcomers"
lang: en
---

This article is a stub. You can help the Fediverse wiki by expanding it. Anyone welcome to contribute.

It may be a question-answer format covering basic things, like:
  - what federated social networks mean
  - where to register
  - tips for choosing a server. May be different for each network: in diaspora one might pay attention to whether a pod uses Camo proxy or not, if it uses the relay, whether it has a Tor .onion address; in Mastodon one can choose an instance by theme, by number of other instances it's connected to... etc. In any network important factors for choosing a pod are pod uptime and time being online. Most of these things are reflected in different server lists.

...May be better link to one of already existing FAQs out there instead of writing a new one. Needs discussion.
