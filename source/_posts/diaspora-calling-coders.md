
---
layout: "post"
title: "diaspora* calling coders"
date: 2018-03-25
tags:
    - diaspora
preview: "As an open-source Free Software project, diaspora* is created by its community. With an influx of new users there's a new call for collaboration."
url: "https://pod.diaspora.software/posts/aa5372c012800136f4ed5254001bd39e"
lang: en
---

It’s been wonderful to see so many new members joining our community over the past few days.

You might already have noticed that diaspora* is missing some features that you’d really like to use.

As an open-source Free Software project, diaspora* is created by its community. Now that you’re part of this community, we’d love you to help us improve it for everyone.

Read the official call for help [here](https://pod.diaspora.software/posts/aa5372c012800136f4ed5254001bd39e).
